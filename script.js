class shape 
{
    constructor(x, y)
    {
        this.x = x;
        this.y = y;
       
            this.calcArea=()=>{return 0}
           this. GetCircumference=()=>{return 0}
    }
    
   
  

}

//----------------------------------------------------------------
class Rectangle extends shape
{
   constructor(x=0, y=0, width, height)
   {
      super(x,y); 
       this.width = width;
       this.height = height;
     this.calcArea=()=>{return this.width * this.height};
   this.GetCircumference=()=>{return  (this.width + this.height)* 2};
   this.classname=this.constructor.name
   }
}

//----------------------------------------------------------------
class square extends Rectangle
{
      
    constructor(x,y,width,height,length){
    super(x,y,width,height);
    this.length = length;
    this.calcArea=()=>{return this.length* this.length};
   this. GetCircumference=()=>{return (this.length +this.length) * 2};
    this.classname=this.constructor.name

    }
    }
   

//----------------------------------------------------------------
class oval extends shape
{
    constructor(x,y,A, B)
    {
        super(x,y);
        this.A = A; this.B = B;
        this.calcArea=()=>{return this.A *this.B *Math.PI};
       this. GetCircumference=()=>{return (this.A +this. B) * Math.PI};
       this.classname=this.constructor.name

    }
   
}
//----------------------------------------------------------------
class Circle extends oval
{

     constructor(x,y,A,B,R)
     { 
         super(x,y,A,B)
         this.R = R
         this.calcArea=()=>{return this.R *this.R *Math.PI};
    this.GetCircumference=()=>{return (this.R + this.R)*Math.PI};
    this.classname=this.constructor.name
     }
   
}
//-----------------------------
class DrawArea
{
    
    constructor(){
       
        this.shapes=[];
        this.add = (shp) => {
            shp.forEach(element => {
                this.shapes.push(element);
            });

        }

       this.log= ()=>
       {
           for(var obj of this.shapes)
           {
            
               console.log(`Circumference=${obj.GetCircumference()},Area=${obj.calcArea()},type=${obj.classname}`);
           }
       }
        
      
}}
 


//------------------------------main
X=0, Y=0, Width = 10 , Height = 5, Length = 5, A = 10, B = 5, R =5


let rec =new Rectangle(0,0,10,5);
let sq=new square(0,0,10,5,5);
let ov=new oval(0,0,10,5);
let circ=new Circle(0,0,10,5,5);

let draw =new DrawArea();

draw.add([rec,sq,ov,circ]);
draw.log();